# Python CI image

Docker image used in Python projects' CI/CD pipelines.

## Available tools

- Latest stable Python 3, pipx and a pipx installed Poetry and Twine.
- Pandoc, Inkscape and TeXLive with default dependencies for compiling documents with RaESL
- PNPM/NodeJS LTS for web projects (such as the ESL JupyterLab extension)
- MkDocs environment with our open-source packages installed
- Some fonts
