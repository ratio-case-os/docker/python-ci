ARG FROM
FROM ${FROM}

# Environment variables
ENV DEBIAN_FRONTEND=noninteractive \
    PIP_NO_CACHE_DIR=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    TYPST_RELEASE=typst-x86_64-unknown-linux-musl.tar.xz \
    USER=ratio \
    UID=1000 \
    GID=100
ENV HOME=/home/${USER}
ENV PNPM_HOME=${HOME}/.local/share/pnpm \
    TYPST_HOME=${HOME}/.local/share/typst \
    FONTS_DIR=${HOME}/.local/share/fonts
ENV PATH=${PNPM_HOME}:/usr/local/texlive/bin/any:${HOME}/.local/bin:${HOME}/bin:${PATH}

# Add Prebuilt-MPR to install the 'just' task runner
RUN apt-get update \
    && apt-get install -qy --no-install-recommends \
    # Aptitude dependencies
    apt-utils \
    build-essential \
    curl \
    fonts-hack \
    fonts-liberation2 \
    git \
    git-lfs \
    gcc \
    gnupg \
    graphviz \
    inkscape \
    jq \
    libc6 \
    libcairo2-dev \
    libffi-dev \
    libfreetype6-dev \
    libjpeg-dev \
    libpng-dev \
    libxml2-dev \
    libxslt1-dev \
    libz-dev \
    lsb-release \
    make \
    nano \
    openssh-client \
    pandoc \
    sudo \
    tar \
    tini \
    unzip \
    wget \
    zsh \
    # Cleanup python temp files.
    && find / -type f -name "*.py[co]" -delete || true \
    && find / -type d -name "__pycache__" -delete || true \
    # Cleanup apt
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

# Setup ${USER}
RUN sed -i 's/^#force_color_prompt=yes/force_color_prompt=yes/' /etc/skel/.bashrc \
    && echo "auth requisite pam_deny.so" >> /etc/pam.d/su \
    && sed -i.bak -e 's/^%admin/#%admin/' /etc/sudoers \
    && sed -i.bak -e 's/^%sudo/#%sudo/' /etc/sudoers \
    && useradd -m -s /bin/bash -N -g ${GID} -u ${UID} ${USER} \
    && chmod g+w /etc/passwd  \
    && echo "PATH=${HOME}/.local/bin:${HOME}/bin:${PATH}" >> /etc/bash.bashrc \
    # Setup TeXLive diurectories.
    && mkdir -p /usr/local/texlive \
    && chown ${UID}:${GID} /usr/local/texlive \
    && mkdir -p ${HOME}/.cache \
    && chown -R ${UID}:${GID} ${HOME}/.cache

# Install all justfiles in this project as scripts.
RUN curl --proto '=https' --tlsv1.2 -sSf https://just.systems/install.sh | bash -s -- --to /usr/local/bin \
    && cd /etc \
    && git clone --depth=1 --single-branch --branch=main https://gitlab.com/ratio-case-os/justfiles.git \
    && cd justfiles \
    && just install

# Switch to regular user.
USER ${USER}

# Setup pnpm/nodejs for web projects
RUN curl -fsSL https://get.pnpm.io/install.sh | SHELL=`which bash` bash -
RUN pnpm config set store-dir ${HOME}/.pnpm-store
RUN pnpm env use --global lts

# Setup Typst CLI
RUN mkdir -p ${TYPST_HOME} \
    && mkdir -p ${HOME}/.local/bin \
    && curl -sSL https://github.com/typst/typst/releases/latest/download/${TYPST_RELEASE} | tar -xJC ${TYPST_HOME} --strip-components 1 \
    && ln -s ${TYPST_HOME}/typst ${HOME}/.local/bin/typst

# Setup Ratio Typst.
RUN THEME_DIR=${TYPST_HOME}/ratio-theme \
    && mkdir -p ${THEME_DIR} \
    && git clone --depth=1 https://gitlab.com/ratio-case-os/typst/ratio-theme.git ${THEME_DIR} \
    && cd ${THEME_DIR} \
    && just install

# Add fonts from api.fontsource.org.
RUN mkdir -p ${FONTS_DIR} \
    && cd ${FONTS_DIR} \
    && curl -o font.zip -L "https://api.fontsource.org/v1/download/fira-code" \
    && unzip font.zip -d ./fira-code \
    && rm font.zip \
    && curl -o font.zip -L "https://api.fontsource.org/v1/download/noto-color-emoji" \
    && unzip font.zip -d ./noto-color-emoji \
    && rm font.zip \
    && curl -o font.zip -L "https://api.fontsource.org/v1/download/noto-emoji" \
    && unzip font.zip -d ./noto-emoji \
    && rm font.zip \
    && curl -o font.zip -L "https://api.fontsource.org/v1/download/noto-sans" \
    && unzip font.zip -d ./noto-sans \
    && rm font.zip \
    && curl -o font.zip -L "https://api.fontsource.org/v1/download/poppins" \
    && unzip font.zip -d ./poppins \
    && rm font.zip \
    && curl -o font.zip -L "https://api.fontsource.org/v1/download/source-sans-3" \
    && unzip font.zip -d ./source-sans-3 \
    && rm font.zip \
    && rm -rf ${FONTS_DIR}/*/webfonts/ \
    && fc-cache -f

# Setup TeXLive as ${USER}
RUN --mount=type=bind,source=/texlive.profile,target=/texlive.profile \
    cd ${HOME} \
    && git config --global credential.helper 'store --file ${HOME}/work/.git-credentials' \
    && git lfs install \
    && mkdir install-tl \
    && curl -SL https://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz | tar zx -C ${HOME}/install-tl --strip-components 1 \
    && cd install-tl \
    && ./install-tl --profile=/texlive.profile \
    && TEXBIN=$(find /usr/local/texlive/bin -maxdepth 1 -type d | tail -n -1) \
    && ln -s ${TEXBIN} /usr/local/texlive/bin/any \
    && echo "PATH=/usr/local/texlive/bin/any:${PATH}" >> ${HOME}/.bashrc \
    && echo "PATH=/usr/local/texlive/bin/any:${PATH}" >> ${HOME}/.zshrc \
    && rm -rf ${HOME}/install-tl
RUN tlmgr install --force \
    appendix \
    adjustbox \
    attachfile \
    babel-dutch \
    booktabs \
    background \
    bidi \
    biber \
    caption \
    catchfile \
    collectbox \
    collcell \
    csquotes \
    csvsimple \
    everypage \
    filehook \
    float \
    footmisc \
    footnotebackref \
    framed \
    fvextra \
    hardwrap \
    ifmtarg \
    koma-script \
    latexmk \
    letltxmacro \
    listings \
    ly1 \
    makecell \
    mdframed \
    mweights \
    needspace \
    pagecolor \
    pgf \
    setspace \
    siunitx \
    sourcecodepro \
    sourcesanspro \
    texliveonfly \
    titling \
    ucharcat \
    ulem \
    unicode-math \
    upquote \
    xcolor \
    xecjk \
    xkeyval \
    xurl \
    zref \
    && find /usr/local/texlive -type f -name "*.log" -delete
COPY --chown=${UID}:${GID} /fonts.conf ${HOME}/.config/fontconfig/fonts.conf

# Setup OhMyZsh as ${USER}.
RUN curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh | bash -s \
    && echo "PATH=${HOME}/.local/bin:${HOME}/bin:${PATH}" >> ${HOME}/.zshrc \
    && sed -i 's/^ZSH_THEME="robbyrussell"/ZSH_THEME="agnoster"/' ${HOME}/.zshrc

# Install pip dependencies as ${USER}.
RUN python3 -m pip install --user pipx \
    && mkdir -p /home/ratio/.cache/pipx \
    && chown ${UID}:${GID} /home/ratio/.cache/pipx \
    && mkdir -p /home/ratio/.local/lib \
    && chown ${UID}:${GID} /home/ratio/.local/lib \
    && pipx ensurepath \
    && pipx install --pip-args=--no-cache uv \
    && pipx install --pip-args=--no-cache poetry \
    && pipx install --pip-args=--no-cache ruff \
    && pipx install --pip-args=--no-cache twine \
    && pipx install --pip-args=--no-cache mkdocs \
    && pipx inject mkdocs \
    cairosvg \
    mkdocs-autorefs \
    mkdocs-bibtex \
    mkdocs-glightbox \
    mkdocs-literate-nav \
    "mkdocs-material[recommended,git,imaging]" \
    "mkdocstrings[python]" \
    pydoc-markdown \
    "raesl[all]" \
    "ragraph[all]" \
    # Cleanup python temp files.
    && find / -type f -name "*.py[co]" -delete || true \
    && find / -type d -name "__pycache__" -delete || true

# Re-enable default bytecode behavior during usage
ENV PYTHONDONTWRITEBYTECODE=
USER ${USER}
VOLUME ${HOME}/work
WORKDIR ${HOME}/work
ENTRYPOINT ["tini", "-g", "--"]
CMD ["python-ci", "default"]
