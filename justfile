set dotenv-load
set export
HERE := env_var_or_default("HERE", invocation_directory())
DOCKERFILE := env_var_or_default("DOCKERFILE", HERE)
FROM := env_var_or_default("FROM", "python:3-slim")
IMAGE := env_var_or_default("IMAGE", "registry.gitlab.com/ratio-case-os/docker/python-ci")
TAG := env_var_or_default("TAG", "latest")
JUSTFILE := justfile()

# Show the recipe list.
default:
  @just --list --justfile {{JUSTFILE}}

# Build the OCI image.
build args="":
  podman build {{DOCKERFILE}} -t {{IMAGE}}:{{TAG}} --build-arg FROM={{FROM}} {{args}}

# Pull the FROM and built OCI image from the registry.
pull args="":
  podman pull {{FROM}} {{args}}
  podman pull {{IMAGE}}:{{TAG}} {{args}}

# Push the OCI image to the registry.
push args="":
  podman push {{IMAGE}}:{{TAG}} {{args}}

# Run the OCI image with the project directory mounted.
run args="" cmd="":
  podman run --privileged --rm -it -v {{HERE}}:/home/ratio/work {{args}} {{IMAGE}}:{{TAG}} {{cmd}}

# Run a recipe in the OCI image.
in recipe="":
  podman run --privileged --rm -it -v {{HERE}}:/home/ratio/work {{IMAGE}}:{{TAG}} just {{recipe}}

dive args="":
  dive {{IMAGE}}:{{TAG}} {{args}}
